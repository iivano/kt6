import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Program implements an application that creates a random simple graph where each vertex has its height
     * attribute. Program solves a problem of searching the safest path from one point to another in graph.
     * Safest path between two points means that paths highest local decline (from one point to another) is
     * the smallest.
     *
     * There's two ways to interpret the solution:
     * 1) if allowed only moving down on the path;
     * 2) if allowed moving up and down on the path;
     * To have an option to implement both ways there is a Graph class method called "setWorkingMode" which takes
     * integer value of mode you want to choose (1 or 2).
     *
     * P.S. if mode 1 is chosen and created arc has a negative decline (decline is calculated as heights difference of
     * two connected vertices) then arc decline between two vertices has a value of 0.
     *
     * Algorithm is based on DFS algorithm logic.
     *
     * @author Ilja Ivanov
     * @author Jaanus Pöial
     * @since 21.11.2020
     * @link source/inspiration https://www.geeksforgeeks.org/depth-first-search-or-dfs-for-a-graph
     *
     * Actual main method to run examples and everything.
     * Before using, it is required to set a working mode of algorithm.
     */
    public void run() {
        Graph g = new Graph("G");
        //Set working mode 1 or 2 (default is 1), full manual in method docstring
        g.setWorkingMode(1);
        //Set maximum possible height of vertices in graph
        g.setHeight(100);

        //Create graph with chosen mode
        g.createRandomSimpleGraph(6, 9);

        System.out.println(g);

        /*
            Set points between which algorithm will find the safest path
            in range from 1 to N, where N is quantity of graph vertices.

            !!!
            Comment in below code if want to use UX and write between which points
            program has to find the safest way
            (recommended if algorithms working mode is set to 1).
        */

//        Scanner scan = new Scanner(System.in);
//        System.out.print("Write id of starting point -> ");
//        int first = scan.nextInt();
//        System.out.print("Write id of finish point -> ");
//        int second = scan.nextInt();
//
//        Vertex start = g.getVertexById(first);
//        Vertex finish = g.getVertexById(second);


        /*
            Those two lines should be commented out if previous option is used
         */


        Vertex start = g.getVertexById(1);
        Vertex finish = g.getVertexById(6);

        ArrayList<Arc> answer = g.safestWaySearch(start, finish);
        System.out.println("The safest way is " + answer);
    }

    class Vertex {
        private final String id;
        private Vertex nextVertex;
        private Arc firstArc;
        private int info = 0;
        private int height = 0;
        private Arc leadingArc = null;
        public boolean visited;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            nextVertex = v;
            firstArc = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private final String id;
        private Vertex targetVertex;
        private Vertex fromVertex;
        private Arc nextArc;
        private int decline = 0;

        Arc(String s, Vertex from, Vertex to, Arc a) {
            id = s;
            fromVertex = from;
            targetVertex = to;
            nextArc = a;
        }

        Arc(String s) {
            this(s, null, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    class Graph {
        private String id;
        private Vertex first;
        private int info = 0;
        private int height = 0;
        private boolean twoDirections = false;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(nl);
            sb.append(id);
            sb.append("\nmax height: " + height);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" height:(");
                sb.append(v.height);
                sb.append(")");
                sb.append(" -->");
                Arc a = v.firstArc;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" decline:(");
                    sb.append(a.decline);
                    sb.append(")");
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.targetVertex.toString());
                    sb.append(")");
                    a = a.nextArc;
                }
                sb.append(nl);
                v = v.nextVertex;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.height = (int) (Math.random() * height);
            res.nextVertex = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.nextArc = from.firstArc;
            from.firstArc = res;
            res.fromVertex = from;
            res.targetVertex = to;
            if (twoDirections) {
                res.decline = Math.abs(from.height - to.height);
            } else {
                res.decline = Math.max(from.height - to.height, 0);
            }
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.nextVertex;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.firstArc;
                while (a != null) {
                    int j = a.targetVertex.info;
                    res[i][j]++;
                    a = a.nextArc;
                }
                v = v.nextVertex;
            }

            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.nextVertex;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Method to find the safest path in the graph
         *
         * @return safest path between two vertexes
         * Side effect: corrupts leadingArc fields in the graph
         **/
        public ArrayList<Arc> safestWaySearch(Vertex st, Vertex fin) {
            if (st.id.equals(fin.id)) return new ArrayList<>();

            // Creating PriorityQueue
            Comparator<Arc> comparator = Comparator.comparingInt(a -> a.decline);
            PriorityQueue<Arc> queue = new PriorityQueue<>(comparator);

            // Add first neighbours to queue
            st.visited = true;
            Arc a = st.firstArc;
            while (a != null) {
                queue.add(a);
                a = a.nextArc;
            }

            // While Fin not found, take lowest priority and process it
            while (queue.peek() != null) {
                Arc now = queue.poll();
                if (now.targetVertex.visited) continue;

                now.targetVertex.visited = true;
                now.targetVertex.leadingArc = now;

                if (now.targetVertex.id.equals(fin.id)) break;

                a = now.targetVertex.firstArc;
                while (a != null) {
                    queue.add(a);
                    a = a.nextArc;
                }
            }

            if (fin.leadingArc == null) return null;

            ArrayList<Arc> answer = new ArrayList<>();
            Vertex now = fin;
            while (now.leadingArc != null) {
                answer.add(now.leadingArc);
                now = now.leadingArc.fromVertex;
            }
            Collections.reverse(answer);

            return answer;
        }

        /**
         * Method to set working mode of algorithm
         * 0 - find safest path between two connected vertices if only moving down is allowed
         * 1 - find safest path between two connected vertices if allowed moving up and down
         *
         * @param mode mode of algorithm process
         */
        public void setWorkingMode(int mode) {
            if (mode == 1) {
                twoDirections = false;
            } else if (mode == 2) {
                twoDirections = true;
            } else {
                throw new RuntimeException("Invalid mode set");
            }
        }

        /**
         * @return number of vertices in graph
         */
        public int countVertices() {
            if (first == null)
                throw new RuntimeException("Graph is not created!");
            int counter = 0;
            Vertex result = first;

            while (result != null) {
                result = result.nextVertex;
                counter++;
            }
            return counter;
        }

        /**
         * Method that returns vertex from created graph with given id
         *
         * @param id of required vertex
         * @return vertex from created graph
         */
        public Vertex getVertexById(int id) {
            if (first == null)
                throw new RuntimeException("Graph is not created!");
            if (id > countVertices()) {
                throw new RuntimeException("Vertex id is out of range");
            }

            Vertex result = first;

            while (!result.id.equals("v" + id)) {
                result = result.nextVertex;
            }

            return result;
        }

        /**
         * Method to set graphs vertices maximum height
         *
         * @param height
         */
        public void setHeight(int height) {
            this.height = height;
        }
    }

}